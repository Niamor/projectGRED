package fr.rg.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Collecter {

	private ArrayList<ZipFileProperty> listZipFileProperty;

	public Collecter(){
		listZipFileProperty = new ArrayList<>();
	}

	public Object load () throws FileNotFoundException, IOException, ClassNotFoundException {
		File f = new File("result.save");
		ObjectInputStream ois;
		if (f.exists())
		{
			ois = new ObjectInputStream( 
					new FileInputStream(
							new File ("result.save")));
			ArrayList<ZipFileProperty> listFile = (ArrayList<ZipFileProperty>) ois.readObject();
			ois.close();
			return listFile;
		} 
		else 
			throw new FileNotFoundException();
	} 

	public void startScan () throws IOException{
		try {	
			for(ZipFileProperty file : (ArrayList<ZipFileProperty>) load()) {
				listZipFileProperty.add(file);
			}
		}catch (IOException e){
			Scanner scan = new Scanner();
			//				BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			//				System.out.println("Choose the type of your document :");
			//				String selectedType = bufferRead.readLine();
			//				System.out.println("Choose the rootPath : ");
			//				String rootPath= bufferRead.readLine();
			DocumentType selectedType = DocumentType.OpenDocumentText;
			String rootPath = "C:/Users/romai_000/Documents/L2S3/Anglais/";
			scan.listFile(selectedType, rootPath);
			for (File file : scan.getList()) {
				listZipFileProperty.add(new ZipFileProperty(file));
			}
			try {
				File f = new File ("result.save");
				ObjectOutputStream oos = new ObjectOutputStream(
						new FileOutputStream(f));
				oos.writeObject(listZipFileProperty);
				oos.close();
			} catch (IOException e2) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e){
			e.printStackTrace();
		}
	}

	public String toString() {
		return "Collecter [listFileProperty=" + listZipFileProperty + "]";
	}


}

